﻿using Domain.Data;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PermissionMock.Repositories
{
    public class MockPermissionRepository : Domain.Repositories.BaseRepository<Domain.Entities.Permission>
    {
        private List<Permission> permissions = new List<Permission>();
        public MockPermissionRepository()
        {
            this.permissions.AddRange(new List<Permission>() {
                new Permission
                {
                    Id = 1,
                    EmployeeName = "Emy",
                    EmployeeLastname = "Soto",
                    PermissionDate = new DateTime(),
                    PermissionTypeId =  (int)PermissionTypeEnum.MarriagePaidLeave,
                    PermissionType =  PermissionTypeEnum.MarriagePaidLeave,
                },
                new Permission
                {
                    Id = 2,
                    EmployeeName = "Marcos",
                    EmployeeLastname = "Pedro",
                    PermissionDate = new DateTime().AddDays(2),
                    PermissionTypeId =  (int)PermissionTypeEnum.SickLeave,
                    PermissionType =  PermissionTypeEnum.SickLeave,
                },
             });
        }
        private static int countId = 3;
        public Task<Permission> CreateAsync(Permission entity)
        {
            entity.Id = countId;
            this.permissions.Add(entity);
            countId++;
            return Task.FromResult(entity);
        }

        public Task DeleteAsync(int entityId)
        {
            var permissionIndex = this.permissions.FindIndex(x => x.Id == entityId);
            if (permissionIndex == -1)
                throw new CustomException(Domain.Constants.ERR_NOT_FOUND, "No se pudo encontrar el registro para ser eliminado. Favor comuniquese con su administrador.");

            return Task.Run(() => this.permissions.RemoveAt(permissionIndex));
        }

        public Task<Permission> FindAsync(int id, Expression<Func<Permission, object>> include = null)
        {
            var result = this.permissions.Find(x => x.Id == id);

            if (result == null)
                throw new CustomException(Domain.Constants.ERR_NOT_FOUND, $"No se pudo encontrar el registro con el id {id}. Favor comuniquese con su administrador.");

            return Task.FromResult(result);
        }

        public Task<Permission> FindAsync(Expression<Func<Permission, bool>> expression, Expression<Func<Permission, object>> include = null)
        {
            return Task.FromResult(this.permissions.AsQueryable().FirstOrDefault(expression));
        }

        public Task<IQueryable<Permission>> FindAllAsync(Expression<Func<Permission, object>> include = null)
        {
            return Task.FromResult(this.permissions.AsQueryable());
        }

        public Task<IQueryable<Permission>> FindByConditionAsync(Expression<Func<Permission, bool>> expression)
        {
            return Task.FromResult(this.permissions.AsQueryable().Where(expression));
        }

        public Task UpdateAsync(Permission entity)
        {
            return Task.Run(() =>
             {
                 var permissionIndex = this.permissions.FindIndex(x => x.Id == entity.Id);
                 this.permissions[permissionIndex] = entity;
                 this.permissions[permissionIndex].LastModificationTime = new DateTime();
             });
        }
    }
}
