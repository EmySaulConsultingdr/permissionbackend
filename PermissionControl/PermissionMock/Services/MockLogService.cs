﻿namespace PermissionMock.Services
{
    public class MockLogService : Domain.Services.LoggerService
    {
        public void LogDebug(string message)
        {
            System.Console.WriteLine(message);
        }

        public void LogError(string message)
        {
            System.Console.WriteLine(message);
        }

        public void LogInfo(string message)
        {
            System.Console.WriteLine(message);
        }

        public void LogWarn(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}
