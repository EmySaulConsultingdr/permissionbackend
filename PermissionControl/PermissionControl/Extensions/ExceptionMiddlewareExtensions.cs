﻿using CoreBasic.CustomExceptionMiddleware;
using Microsoft.AspNetCore.Builder;

namespace CoreBasic.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
