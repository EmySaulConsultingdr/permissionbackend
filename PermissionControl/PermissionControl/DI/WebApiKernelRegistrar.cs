﻿using DI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CoreBasic.DI
{
    public class WebApiKernelRegistrar : KernelRegistrar
    {
        IServiceCollection _serviceCollection;
        IConfiguration _configuration;
        public WebApiKernelRegistrar(IServiceCollection services, IConfiguration configuration)
        {
            this._serviceCollection = services;
            this._configuration = configuration;
        }
        public void RegisterAsDbContext<TContractType, TDbContext>(string contextName) where TDbContext : DbContext, TContractType
        {
            _serviceCollection.AddDbContext<TContractType, TDbContext>(o =>
            {
                o.UseSqlServer(_configuration.GetConnectionString(contextName));
            });
        }

        public void RegisterAsScope(Type contractType, Type implementationType)
        {
            _serviceCollection.AddScoped(contractType, implementationType);
        }

        public void RegisterAsScope<ContractType, Implementation>(string alias = null)
            where ContractType : class
            where Implementation : class, ContractType
        {
            _serviceCollection.AddScoped<ContractType, Implementation>();
        }

        public void RegisterAsSingleton<ContractType, Implementation>(string alias)
            where ContractType : class
            where Implementation : class, ContractType
        {
            _serviceCollection.AddSingleton<ContractType, Implementation>();
        }

        public void RegisterAsSingleton(Type contractType, Type implementationType)
        {
            _serviceCollection.AddSingleton(contractType, implementationType);
        }

        public void RegisterAsSingleton<TServices>(TServices implementationType) where TServices : class
        {
            _serviceCollection.AddSingleton(implementationType);
        }
    }
}
