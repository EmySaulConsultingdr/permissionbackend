﻿using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoreBasic.Controllers
{
    [Route("api/permission-types")]
    [ApiController]
    public class PermissionTypeController : CustomBaseController<Domain.Entities.PermissionType, Domain.Models.PermissionType>
    {
        public PermissionTypeController(BaseProvider<Domain.Entities.PermissionType, Domain.Models.PermissionType> provider,
                                        BaseService<Domain.Entities.PermissionType, Domain.Models.PermissionType> service,
                                        LoggerService logger)
                                        : base(provider, service, logger)
        {
        }
    }
}
