﻿using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoreBasic.Controllers
{
    [Route("api/permissions")]
    [ApiController]
    public class PermissionController : CustomBaseController<Domain.Entities.Permission, Domain.Models.Permission>
    {
        public PermissionController(PermissionProvider provider,
                                        PermissionService service, LoggerService logger) 
                                        : base(provider, service, logger)
        {
        }
    }
}
