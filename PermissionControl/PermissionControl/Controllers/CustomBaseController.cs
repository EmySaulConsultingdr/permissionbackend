﻿using Domain.Entities;
using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreBasic.Controllers
{
    public abstract class CustomBaseController<TEntity, TModel> : ControllerBase where TEntity : Entity where TModel : class
    {
        private readonly BaseProvider<TEntity, TModel> _provider;
        private readonly BaseService<TEntity, TModel> _service;
        private readonly LoggerService _logger;
        public CustomBaseController(BaseProvider<TEntity, TModel> provider,
                                        BaseService<TEntity, TModel> service,
                                        LoggerService logger)
        {
            _provider = provider;
            _service = service;
            _logger = logger;
        }


        [HttpGet()]
        public async Task<ActionResult<IEnumerable<TModel>>> GetAll()
        {
            return Ok(await _provider.GetAllAsync());
        }


        [HttpGet("{id:int}")]
        public async Task<ActionResult<TModel>> Get([FromRoute] int id)
        {
            return Ok(await _provider.GetAsync(id));
        }

        [HttpPost()]
        public async Task<ActionResult<int>> Create(TModel model)
        {
            return Ok(await _service.CreateAsync(model));
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> Update(int id, TModel model)
        {
            await _service.UpdateAsync(id, model);
            return Ok();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteAsync(id);
            return Ok();
        }
    }
}
