﻿using DatabaseContracts;
using Domain.Entities;
using EntityFramework.Configurations;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace EntityFramework.Contexts
{
    public class PermissionDbContext : DbContext, DbContextContract
    {
        public PermissionDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        public DbSet<Permission> Permissions { get; set; }
        public DbSet<PermissionType> PermissionTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PermissionTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
