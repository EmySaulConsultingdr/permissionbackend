﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace EntityFramework.Configurations
{
    public class PermissionTypeConfiguration : IEntityTypeConfiguration<PermissionType>
    {
        public void Configure(EntityTypeBuilder<PermissionType> builder)
        {
            builder.HasData(GetSeedPermissionTypes());
        }

        public static IEnumerable<PermissionType> GetSeedPermissionTypes()
        {
            foreach (PermissionTypeEnum @enum in Enum.GetValues(typeof(PermissionTypeEnum)))
                yield return @enum;
        }
    }
}
