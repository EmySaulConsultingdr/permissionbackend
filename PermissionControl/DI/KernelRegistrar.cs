﻿using Microsoft.EntityFrameworkCore;
using System;

namespace DI
{
    public interface KernelRegistrar
    {
        void RegisterAsScope(Type contractType, Type implementationType);
        void RegisterAsDbContext<TContractType, TDbContext>(string contextName) 
            where TDbContext : DbContext, TContractType;
        void RegisterAsScope<ContractType, Implementation>(string alias = null)
           where ContractType : class
           where Implementation : class, ContractType;
        void RegisterAsSingleton<ContractType, Implementation>(string alias = null)
          where ContractType : class
          where Implementation : class, ContractType;
        void RegisterAsSingleton(Type contractType, Type implementationType);
        void RegisterAsSingleton<TServices>(TServices implementationType) where TServices : class;

    }
}
