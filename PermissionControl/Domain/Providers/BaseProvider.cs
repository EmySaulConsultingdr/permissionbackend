﻿using Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Providers
{
    public interface BaseProvider<TEntity, TModel> where TEntity : Entity where TModel : class
    {
        Task<TModel> GetAsync(int id);
        Task<IEnumerable<TModel>> GetAllAsync();
    }
}
