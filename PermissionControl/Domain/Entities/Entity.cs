﻿using System;

namespace Domain.Entities
{
    public class Entity
    {
        public virtual int Id { get; set; }
        public DateTime CreatedAt { get; private set; } = new DateTime();
        public DateTime LastModificationTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}