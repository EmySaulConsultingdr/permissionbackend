﻿using Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Permission : Entity
    {
        public string EmployeeName { get; set; }
        public string EmployeeLastname { get; set; }
        public int PermissionTypeId { get; set; }
        public DateTime PermissionDate { get; set; }

        [ForeignKey(nameof(PermissionTypeId))]
        public virtual PermissionType PermissionType { get; set; }
    }
}
