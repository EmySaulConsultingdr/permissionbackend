﻿using Domain.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class PermissionType : Entity
    {
        public PermissionType(PermissionTypeEnum @enum)
        {
            Id = (int)@enum;
            Description = @enum.GetEnumDescription();
        }

        public PermissionType() { } // For EF

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override int Id { get; set; }
        public string Description { get; set; }

        public static implicit operator PermissionType(PermissionTypeEnum @enum) => new PermissionType(@enum);

        public static implicit operator PermissionTypeEnum(PermissionType orderStatus) => (PermissionTypeEnum)orderStatus.Id;
    }
}
