﻿namespace Domain
{
    public static class Constants
    {
        public const int ERR_CREATING = 1000;
        public const int ERR_READING = 1001;
        public const int ERR_UPDATING = 1003;
        public const int ERR_DELETING = 1004;
        public const int ERR_NOT_FOUND = 1005;
    }

}