﻿using System;

namespace Domain.Data
{
    public class CustomException : Exception
    {
        public int CustomCode;
        public object ExceptionData;

        public CustomException()
       : base() { }

        public CustomException(string message)
            : base(message)
        {
        }

        public CustomException(int customCode, string message) : base(message)
        {
            this.CustomCode = customCode;
        }

        public CustomException(int customCode, string message, Exception exception) : base(message, exception)
        {
            this.CustomCode = customCode;
        }
    }
}
