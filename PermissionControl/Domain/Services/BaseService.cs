﻿using Domain.Entities;
using System.Threading.Tasks;

namespace Domain.Services
{
    public interface BaseService<TEntity, TModel> where TEntity : Entity where TModel : class
    {
        Task<int> CreateAsync(TModel model);
        Task DeleteAsync(int id);
        Task UpdateAsync(int id, TModel model);
    }
}
