﻿namespace Domain.Models
{
    [Attributes.AutoMap(typeof(Entities.PermissionType))]
    public class PermissionType
    {
        public int? Id { get; set; }
        public string Description { get; set; }
    }
}
