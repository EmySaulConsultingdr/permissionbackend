﻿using Domain.Attributes;
using System;

namespace Domain.Models
{
    [AutoMap(typeof(Domain.Entities.Permission))]
    public class Permission
    {
        public int? Id { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeLastname { get; set; }
        public int PermissionTypeId { get; set; }
        public DateTime PermissionDate { get; set; }

        public Domain.Models.PermissionType PermissionType { get; set; }
    }
}
