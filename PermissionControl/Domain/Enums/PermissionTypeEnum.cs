﻿using System.ComponentModel;

namespace Domain.Enums
{
    public enum PermissionTypeEnum : int
    {
        [Description("Enfermedad")]
        SickLeave = 1,

        [Description("Diligencias")]
        PersonalProceedings = 2,

        [Description("Permiso retribuido por matrimonio")]
        MarriagePaidLeave = 3,

        [Description("fallecimiento, accidente o enfermedad grave de un familiar.")]
        UnfortunateEventLeave = 4
    }
}
