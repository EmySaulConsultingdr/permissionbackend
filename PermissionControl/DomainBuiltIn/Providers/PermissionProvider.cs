﻿using AutoMapper;
using Domain.Entities;
using Domain.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainBuiltIn.Providers
{
    public class PermissionProvider : BuiltInBaseProvider<Domain.Entities.Permission, Domain.Models.Permission>, Domain.Providers.PermissionProvider

    {
        public PermissionProvider(BaseRepository<Permission> baseRepository, IMapper mapper) : base(baseRepository, mapper)
        {
        }

        public override async Task<Domain.Models.Permission> GetAsync(int id)
        {
            var permission = await _baseRepository.FindAsync(id, x => x.PermissionType);
            return _mapper.Map<Domain.Models.Permission>(permission);
        }
        public override async Task<IEnumerable<Domain.Models.Permission>> GetAllAsync()
        {
            var permissions = await _baseRepository.FindAllAsync(x => x.PermissionType);
            return _mapper.Map<List<Domain.Models.Permission>>(permissions.ToList());
        }
    }
}
