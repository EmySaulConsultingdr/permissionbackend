﻿using AutoMapper;
using Domain.Data;
using Domain.Entities;
using Domain.Enums;
using Domain.Repositories;
using System;
using System.Threading.Tasks;

namespace DomainBuiltIn.Services
{
    public class PermissionService : BuiltInBaseService<Domain.Entities.Permission, Domain.Models.Permission>, Domain.Services.PermissionService
    {
        public PermissionService(BaseRepository<Permission> entityRepository, IMapper mapper) : base(entityRepository, mapper)
        {
        }

        public override Task<int> CreateAsync(Domain.Models.Permission model)
        {
            if (!Enum.IsDefined(typeof(PermissionTypeEnum), model.PermissionTypeId))
                throw new CustomException(Domain.Constants.ERR_NOT_FOUND, "Tipo de permiso no definido.");

            return base.CreateAsync(model);
        }

        public override async Task UpdateAsync(int id, Domain.Models.Permission model)
        {
            var permission = await _entityRepository.FindAsync(id);

            permission.EmployeeName = model.EmployeeName;
            permission.EmployeeLastname = model.EmployeeLastname;
            permission.PermissionTypeId = model.PermissionTypeId;
            permission.PermissionDate = model.PermissionDate;

            await _entityRepository.UpdateAsync(permission);
        }
    }
}
